#ifndef _FGMRANK_H
#define _FGMRANK_H

// fgmrank.h
//
// FGMatchRanking
//
// Class encapsulating "rank" given to a filename. Typically, the newer
// the version specified within the filename, the higher the rank

class FGMatchRanking {
public:
  // Default constructor
  FGMatchRanking();

  // Need these for the mm
  ~FGMatchRanking();
  FGMatchRanking(const FGMatchRanking& other);
  FGMatchRanking& operator=(const FGMatchRanking& other);

  // Aside from quality of match, was this a match at all?
  bool IsMatch(void) const;

  // Compare two rankings
  bool operator<(const FGMatchRanking& other) const;

  // Stuff another value on the stack
  void AddValue(int value);

  // Set the match flag
  void SetMatch(void);

private:
  // That fabled Cheshire Cat to hide implementation details again
  struct Internal_FGMatchRanking;
  Internal_FGMatchRanking* mpInternals;
};

#endif // _FGMRANK_H
