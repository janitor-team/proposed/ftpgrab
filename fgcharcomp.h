#ifndef _FGCHARCOMP_H
#define _FGCHARCOMP_H

// fgcharcomp.h
//
// FGCharacterComponent: handles ranked matching of a single character,
// with disregard to case. Useful when stuff is released with a name like
// e.g. samba-2.0.5a.tar.gz

#include "fgfncomp.h"

class FGCharacterComponent : public FGFileNameComponent {
public:
  // Constructor
  FGCharacterComponent(bool isOptional);

  virtual bool MatchAndRankComponent(FGString& fnameRemainder,
                                     int* pMatchVal) const;
  virtual ~FGCharacterComponent();

private:
  // Banned!
  FGCharacterComponent(const FGCharacterComponent& other);
  FGCharacterComponent& operator=(const FGCharacterComponent& other);

  bool mIsOptional;
};

#endif // _FGCHARCOMP_H
