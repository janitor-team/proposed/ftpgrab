#ifndef _FGDELACTION_H
#define _FGDELACTION_H

// fgdelaction.h
//
// Implementation class of the download action

#include "fgactioni.h"

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGConnectionInterface;

class FGDeleteAction : public FGActionInterface {
public:
  // Construct with filename to download
  FGDeleteAction(const FGString& fname, const FGString& localDir);

  ~FGDeleteAction();

  // Overridden virtual Do method
  virtual void VirtualDo(void) const;

  // And Abort()
  virtual void Abort(void) const;

private:
  // Banned!
  FGDeleteAction(const FGDeleteAction& other);
  FGDeleteAction& operator=(const FGDeleteAction& other);

  FGString mFileName;
  FGString mLocalDir;
};

#endif // _FGDELACTION_H
