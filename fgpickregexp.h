#ifndef _FGPICKREGEXP_H
#define _FGPICKREGEXP_H

// fgpickregexp.h
//
// Implementation of a file picker that selects all remote files matching
// a given regular expression for which we do not have remote copies

#ifndef _FGFPICKI_H
#include "fgfpicki.h"
#endif

class FGConnectionInterface;
class FGString;

// Ah the joys of c++ forward declarations
struct re_pattern_buffer;
typedef struct re_pattern_buffer regex_t;

class FGPickRegexp : public FGFilePickerInterface {
public:
  // Must be constructed with connection interface and regexp match string
  FGPickRegexp(FGConnectionInterface* pConnIf,
               const FGString& regexp);
  virtual ~FGPickRegexp();

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir);

private:
  // Banned!
  FGPickRegexp(const FGPickRegexp& other);
  FGPickRegexp& operator=(const FGPickRegexp& other);

  // Internal representation of a regexp
  regex_t* mpRegExp;
};

#endif // _FGPICKREGEXP_H
