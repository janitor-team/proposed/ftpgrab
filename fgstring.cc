// fgstring.cc

#include "fgstring.h"

#include <string.h>
#include <assert.h>
#include <stdlib.h>

FGString::FGString()
: mpString(0), mLength(0)
{
}

FGString::FGString(const FGString& other)
{
  mLength = other.GetLength();
  if (mLength) {
    mpString = new char[mLength+1];
    strcpy(mpString, other);
  } else {
    mpString = 0;
  }
}

FGString&
FGString::operator=(const FGString& other)
{
  if (this == &other) {
    return *this;
  }

  if (mpString) {
    delete [] mpString;
  }

  mLength = other.GetLength();
  if (mLength) {
    mpString = new char[mLength+1];
    strcpy(mpString, other);
  } else {
    mpString = 0;
  }

  return *this;
}

FGString::~FGString()
{
  if (mpString) {
    delete [] mpString;
  }
}

FGString::FGString(const char* pStr)
{
  mLength = strlen(pStr);
  mpString = new char[mLength+1];
  strcpy(mpString, pStr);
}

FGString::operator const char*() const
{
  return mpString;
}

unsigned int
FGString::GetLength(void) const
{
  return mLength;
}

void
FGString::operator+=(const FGString& other)
{
  mLength = mLength + other.GetLength();
  char* pTmp = new char[mLength+1];
  if (mpString) {
    strcpy(pTmp, mpString);
    strcat(pTmp, other);
    delete [] mpString;
  } else {
    strcpy(pTmp, other);
  }

  mpString = pTmp;
}

void
FGString::operator+=(char other)
{
  mLength++;
  char* pTmp = new char[mLength+1];
  if (mpString) {
    strcpy(pTmp, mpString);
    pTmp[mLength-1] = other;
    pTmp[mLength] = '\0';
    delete [] mpString;
  } else {
    pTmp[0] = other;
    pTmp[1] = '\0';
  }

  mpString = pTmp;
}

bool
FGString::operator==(const FGString& other) const
{
  if (!mpString || !other.mpString) {
    return false;
  }

  return (strcmp(mpString, other.mpString) == 0);
}

bool
FGString::operator!=(const FGString& other) const
{
  return !(*this == other);
}

bool
FGString::IsEmpty(void) const
{
  if (!mpString || !mLength) {
    return true;
  }
  return false;
}

void
FGString::MakeEmpty(void)
{
  if (mpString) {
    delete [] mpString;
    mpString = 0;
    mLength = 0;
  }
}

char
FGString::operator[](int pos) const
{
  assert(pos >= 0 && (unsigned int) pos <= mLength);

  return mpString[pos];
}

FGString
FGString::Left(unsigned int num) const
{
  if (num == 0) {
    FGString ret;
    return ret;
  }
  if (num >= mLength) {
    return *this;
  }

  // Oh, we have to do some work
  FGString ret;
  ret.mLength = num;
  ret.mpString = new char[num+1];
  strncpy(ret.mpString, mpString, num);
  ret.mpString[num] = '\0';

  return ret;
}

FGString
FGString::Right(unsigned int num) const
{
  if (num == 0) {
    FGString ret;
    return ret;
  }
  if (num >= mLength) {
    return *this;
  }

  // Oh, we have to do some work
  FGString ret;
  ret.mLength = num;
  ret.mpString = new char[num+1];
  strncpy(ret.mpString, mpString + mLength - num, num);
  ret.mpString[num] = '\0';

  return ret;
}

int
FGString::AToI(void) const
{
  return atoi(mpString);
}

bool
FGString::Contains(char c) const
{
  if (strchr(mpString, c))
  {
    return true;
  }

  return false;
}

