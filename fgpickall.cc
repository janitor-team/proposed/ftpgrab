// fgpickall.cc

#include "fgpickall.h"

// Dir listings
#ifndef _FGDLIST_H
#include "fgdlist.h"
#endif

// Action lists
#ifndef _FGALIST_H
#include "fgalist.h"
#endif

// The download file action
#ifndef _FGDLACTION_H
#include "fgdlaction.h"
#endif

FGPickAll::FGPickAll(FGConnectionInterface* pConnIf)
: FGFilePickerInterface(pConnIf)
{
}

FGActionList
FGPickAll::DecideActions(const FGDirListing& localDir,
                         const FGDirListing& remoteDir)
{
  // The picker for the "normal" wildcard operation (non recursive),
  // leaves all local files intact
  // Very simple rule: grab any file in the remoteDir that is
  // not in the localDir
  FGActionList ret;

  // Get list of files in remote but not local
  FGDirListing newFiles = FGDirListing::GetRHSOnlyFiles(localDir, remoteDir);

  // Make a "download" action for all the above files
  std::vector<FGFileInfo>::const_iterator iFiles;
  for (iFiles = newFiles.begin(); iFiles != newFiles.end(); iFiles++) {
    FGActionInterface* pNewAction;

    pNewAction = new FGDownloadAction(iFiles->GetFileName(), mpConnIf,
                                      localDir.GetDirName(),
                                      iFiles->GetSize());
    ret.push_back(pNewAction);
  }

  return ret;
}
