Source: ftpgrab
Maintainer: Christian T. Steigies <cts@debian.org>
Section: net
Priority: optional
Build-Depends: debhelper (>= 11~)
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/cts/ftpgrab
Vcs-Git: https://salsa.debian.org/cts/ftpgrab.git

Package: ftpgrab
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: file mirroring utility
 ftpgrab is a utility for maintaining FTP mirrors. In fact not unlike the
 "Mirror" perl program. However ftpgrab is oriented towards the smaller
 site which doesn't have the resources to mirror entire version trees of
 software.
 .
 The primary "plus point" of ftpgrab is that it can base download decisions
 by parsing version numbers out of filenames. For example, ftpgrab will
 recognize that the file "linux-2.2.2.tar.gz" is newer than
 "linux-2.2.1.tar.gz" based on the version string. It will then download
 the new version and delete the old one when it is done, thus saving you
 mirroring 10 kernel versions all at >10Mb each.
