#ifndef _FGFSHELP_H
#define _FGFSHELP_H

// fgfshelp.h
//
// Helper class for local filesystem operations; static members only

class FGDirListing;
class FGString;

class FGFileSystemHelper {
public:
  // Get a _local_ dir listing
  static FGDirListing GetDirListing(const FGString& dir);

  // Transfer data from one (remote) fd to another
  static void TransferRemoteToLocal(int fromFD, int toFD, int bytes);

  // Create local file to put data in
  static int CreateLocalFile(const FGString& file);

  // Delete a local file
  static void DeleteFile(const FGString& file);

private:
  // Constants
  static const int msTransferBufSize = 1024 * 64;
};

#endif // _FGFSHELP_H
