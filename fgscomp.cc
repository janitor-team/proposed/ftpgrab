// fgscomp.cc

#include "fgscomp.h"

FGStringComponent::FGStringComponent(const FGString& str)
: mStringBit(str)
{
}

FGStringComponent::~FGStringComponent()
{
}

bool
FGStringComponent::MatchAndRankComponent(FGString& fnameRemainder,
                                         int* pMatchVal) const
{
  // Match of a string fragment will always be valueless
  *pMatchVal = -1;

  // No match if our fragment is bigger than what's left
  int fragLen = mStringBit.GetLength();
  int compLen = fnameRemainder.GetLength();

  if (fragLen > compLen) {
    return false;
  }

  // Do comparison
  FGString leftBit = fnameRemainder.Left(fragLen);

  if (mStringBit != leftBit) {
    return false;
  }

  // OK so its a match
  // Truncate the filename remainder accordingly
  FGString fnameLeft = fnameRemainder.Right(compLen - fragLen);
  fnameRemainder = fnameLeft;

  return true;
}
