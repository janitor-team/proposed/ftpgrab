#ifndef _FGALIST_H
#define _FGALIST_H

// fgalist.h
//
// FGActionList - details actions to be taken (if any) to satisfy
// a given grab rule e.g. fetch file A, delete file B

#ifndef __SGI_STL_VECTOR_H
#include <vector>
#endif

class FGActionInterface;

class FGActionList : public std::vector<FGActionInterface*> {
public:
  void DoActions(void) const;
  void FreeResources(void);
};

#endif // _FGALIST_H
