#ifndef _FGICOMP_H
#define _FGICOMP_H

// fgicomp.h
//
// FGIntegerComponent: handles matching of numeric bits of filename.
// Probably the most used ranking primitive; e.g. linux-2.2.<x>.tar.gz
// and 2.2.3 is newer than 2.2.2 :-)

#include "fgfncomp.h"

class FGIntegerComponent : public FGFileNameComponent {
public:
  // Default constructor
  FGIntegerComponent();

  virtual bool MatchAndRankComponent(FGString& fnameRemainder,
                                     int* pMatchVal) const;
  virtual ~FGIntegerComponent();

private:
  // Banned!
  FGIntegerComponent(const FGIntegerComponent& other);
  FGIntegerComponent& operator=(const FGIntegerComponent& other);
};

#endif // _FGICOMP_H
