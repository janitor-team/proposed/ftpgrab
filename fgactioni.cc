// fgactioni.cc

#include "fgactioni.h"

#ifndef _FGEXC_H
#include "fgexc.h"
#endif

FGActionInterface::~FGActionInterface()
{
}

void
FGActionInterface::Do(void) const
{
  try {
    VirtualDo();
  }
  // Any other type of exception is _real_ serious
  catch (FGException& e) {
    if (e.GetReason() != FGException::kConcurrentGrabs) {
      Abort();
    }
    throw;
  }
}
