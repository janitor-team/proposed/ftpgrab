CC	=	g++
CFLAGS	=	-O2 -Wall

#LIBS	=	-lefence
LIBS	=	-lpthread
LINK	=	-Wl,-s 

OBJS	=	main.o fgstring.o fgexc.o fgftpcon.o fgfileinfo.o \
		fgfilegrab.o fgalist.o fgfshelp.o fgpickall.o \
		fgdlist.o fgdlaction.o fgactioni.o fgfilelist.o \
		fgpickallsync.o fgdelaction.o fgfpicki.o fgpickbest.o \
		fgscomp.o fgicomp.o fgbdfname.o fgmrank.o fglogger.o \
		fgdyymmddcomp.o fgfncomp.o fgpickregexp.o \
		fgpickregexpsync.o fgconi.o fgcharcomp.o

.cc.o:
		$(CC) -c $*.cc $(CFLAGS)

ftpgrab:	$(OBJS)
		$(CC) -o ftpgrab $(OBJS) $(LINK) $(LIBS)

clean:
		rm -f *.o ftpgrab fglog.out
