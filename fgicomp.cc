// fgicomp.cc

#include "fgicomp.h"

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

#include <ctype.h>

FGIntegerComponent::FGIntegerComponent()
{
}

FGIntegerComponent::~FGIntegerComponent()
{
}

bool
FGIntegerComponent::MatchAndRankComponent(FGString& fnameRemainder,
                                          int* pMatchVal) const
{
  // Initialize to be safe
  *pMatchVal = -1;

  // Sanity checks
  if (fnameRemainder.GetLength() < 1 || !isdigit(fnameRemainder[0])) {
    return false;
  }

  unsigned int digits = 0;
  // Build up the number here
  FGString theInt;

  do {
    theInt += fnameRemainder[digits];
    digits++;
  } while (digits < fnameRemainder.GetLength() &&
           isdigit(fnameRemainder[digits]));

  // Set the match value
  *pMatchVal = theInt.AToI();

  // Chop off the digits we consumed
  int charsLeft = fnameRemainder.GetLength() - digits;
  FGString bitLeft = fnameRemainder.Right(charsLeft);

  fnameRemainder = bitLeft;

  return true;
}
