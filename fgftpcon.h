#ifndef _FGFTPCON_H
#define _FGFTPCON_H

// fgftpcon.h
//
// FTPGrabFTPConnection
//
// Class implementing getting of files over FTP protocol

#ifndef _FGCONI_H
#include "fgconi.h"
#endif

// Fun and games!!
struct _IO_FILE;
typedef struct _IO_FILE FILE;

class FGFTPCon : public FGConnectionInterface {
public:
  // Default constructor
  FGFTPCon();

  // Destructor
  ~FGFTPCon();

  // Interface methods from base class
  virtual void Connect(const FGString& host);
  virtual void ChangeDir(const FGString& dir);
  virtual FGDirListing GetDirListing(void);
  virtual int GetFile(const FGString& file);
  virtual void PostFileTransfer(void);
  virtual void Disconnect(void);

private:
  // Banned!
  FGFTPCon(const FGFTPCon& other);
  FGFTPCon& operator=(const FGFTPCon& other);

  // Implementation details

  bool mConnected;

  // File descriptor of connected command stream
  int mCommandFD;

  // Stream version of the above
  FILE* mpCommandStream;

  // Buffer for storing/parsing the FTP server replies
  char mBuf[1024];

  // Value of FTP server's reply
  int mResponse;

  // Details for remote data connection
  int mDataFD;
  int mRemotePort;
  FILE* mpDirStream;
  unsigned long mRemoteAddr;

  // Internal constants
  static const int msFTPPort;

  static const int msRespPassRequired;
  static const int msRespBadLogin;
  static const int msRespLoginOK;
  static const int msRespNotFound;
  static const int msRespTransferDone;
  static const int msRespChangeDirOK;
  static const int msRespCommandOK;
  static const int msRespGoodbye;
  static const int msRespPleaseLogin;
  static const int msRespPassiveOK;
  static const int msRespTransferOpen;

  // Internal functions
  void GetResponse(void);
  void GetLine(void);
  void SendCommand(const FGString& cmd) const;
  void SetupPassivePort(void);
  void InternalConnect(int* pDestFD, int port, void* pInetAddr);
};

#endif // _FGFTPCON_H
