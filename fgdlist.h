#ifndef _FGDLIST_H
#define _FGDLIST_H

// fgdlist.h

#ifndef _FGFILEINFO_H
#include "fgfileinfo.h"
#endif

#ifndef __SGI_STL_VECTOR_H
#include <vector>
#endif

#ifndef _FGSTRING_H
#define "fgstring.h"
#endif

class FGDirListing : public std::vector<FGFileInfo> {
public:
  // Constructor must specify the name of the dir
  FGDirListing(const FGString& dirName);

  // Some static helper methods for comparing file lists etc.
  static FGDirListing GetRHSOnlyFiles(const FGDirListing& lhs,
                                      const FGDirListing& rhs);

  const FGString& GetDirName(void) const;

private:
  // Directory name (full path) this listing is actually for
  FGString mDirName;
};

#endif // _FGDLIST_H
