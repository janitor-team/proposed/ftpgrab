#ifndef _FGSCOMP_H
#define _FGSCOMP_H

// fgscomp.h
//
// FGStringComponent: handles matching of plain string bits of a filename

#include "fgfncomp.h"

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGStringComponent : public FGFileNameComponent {
public:
  // Construct from string fragment
  FGStringComponent(const FGString& str);

  virtual bool MatchAndRankComponent(FGString& fnameRemainder,
                                     int* pMatchVal) const;
  virtual ~FGStringComponent();

private:
  // Banned!
  FGStringComponent(const FGStringComponent& other);
  FGStringComponent& operator=(const FGStringComponent& other);

  FGString mStringBit;
};

#endif // _FGSCOMP_H
