// fgpickregexpsync.cc

#include "fgpickregexpsync.h"

// Dir listings
#ifndef _FGDLIST_H
#include "fgdlist.h"
#endif

// Action lists
#ifndef _FGALIST_H
#include "fgalist.h"
#endif

// The download file action
#ifndef _FGDLACTION_H
#include "fgdlaction.h"
#endif

// The delete file action
#ifndef _FGDELACTION_H
#include "fgdelaction.h"
#endif

#ifndef _FGEXC_H
#include "fgexc.h"
#endif

#include <regex.h>

FGPickRegexpSync::FGPickRegexpSync(FGConnectionInterface* pConnIf,
                                   const FGString& regexp)
: FGFilePickerInterface(pConnIf), mpRegExp(0)
{
  mpRegExp = new regex_t;

  try {
    int ret = regcomp(mpRegExp, regexp, REG_EXTENDED | REG_NOSUB);
    if (ret != 0) {
      throw FGException(FGException::kInvalidRegexp);
    }
  }
  catch (FGException&) {
    delete mpRegExp;
    throw;
  }
}

FGPickRegexpSync::~FGPickRegexpSync()
{
  if (mpRegExp != 0) {
    regfree(mpRegExp);
    delete mpRegExp;
  }
}


FGActionList
FGPickRegexpSync::DecideActions(const FGDirListing& localDir,
                                const FGDirListing& remoteDir)
{
  // The picker for the "normal" operation (non recursive),
  // leaves all local files intact
  // Very simple rule: grab any file in the remoteDir that is
  // not in the localDir and matches regexp
  FGActionList ret;

  // Get list of files in local but not remote
  FGDirListing staleFiles = FGDirListing::GetRHSOnlyFiles(remoteDir, localDir);

  std::vector<FGFileInfo>::const_iterator iFiles;

  // Pare down list to only regexp matches
  FGDirListing finalWasteList("NOTUSED");
  for (iFiles = staleFiles.begin(); iFiles != staleFiles.end(); iFiles++) {
    int match = regexec(mpRegExp, iFiles->GetFileName(), 0, 0, 0);
    if (match == 0) {
      finalWasteList.push_back(*iFiles);
    }
  }

  // For each stale file, make an "erase" action
  for (iFiles = finalWasteList.begin(); iFiles != finalWasteList.end();
       iFiles++) {
    FGActionInterface* pNewAction;

    pNewAction = new FGDeleteAction(iFiles->GetFileName(),
                                    localDir.GetDirName());
    ret.push_back(pNewAction);
  }

  // Get list of files in remote but not local
  FGDirListing newFiles = FGDirListing::GetRHSOnlyFiles(localDir, remoteDir);

  // Pare down listing to only regexp matches
  FGDirListing finalList("NOTUSED");
  for (iFiles = newFiles.begin(); iFiles != newFiles.end(); iFiles++) {
    int match = regexec(mpRegExp, iFiles->GetFileName(), 0, 0, 0);
    if (match == 0) {
      finalList.push_back(*iFiles);
    }
  }

  // Make a "download" action for all the above files
  for (iFiles = finalList.begin(); iFiles != finalList.end(); iFiles++) {
    FGActionInterface* pNewAction;

    pNewAction = new FGDownloadAction(iFiles->GetFileName(), mpConnIf,
                                      localDir.GetDirName(),
                                      iFiles->GetSize());
    ret.push_back(pNewAction);
  }

  return ret;
}
