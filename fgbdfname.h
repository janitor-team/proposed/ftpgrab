#ifndef _FGBDFNAME_H
#define _FGBDFNAME_H

// fgbdfname.h
//
// FGBrokenDownFileName
//
// Class representing a filename broken down into components which
// typically consist of strings and version numbers

class FGString;
class FGMatchRanking;

class FGBrokenDownFileName {
public:
  // Constructor - takes in filename to parse into components
  FGBrokenDownFileName(const FGString& fname);

  FGMatchRanking GetRanking(const FGString& cmp) const;

  // Destructor
  ~FGBrokenDownFileName();

private:
  // Banned!!
  FGBrokenDownFileName(const FGBrokenDownFileName& other);
  FGBrokenDownFileName& operator=(const FGBrokenDownFileName& other);

  // Cheshire Cat to hide internals
  struct Internal_FGBrokenDownFileName;
  Internal_FGBrokenDownFileName* mpInternals;
};

#endif // _FGBDFNAME_H
