#ifndef _FGFNCOMP_H
#define _FGFNCOMP_H

// fgfncomp.h
//
// FGFileNameComponent: base class for "bits" of a filename which can
// be compared, matched, ranked etc. to determine how "recent" a file is

class FGString;

class FGFileNameComponent {
public:
  // In: string with remainder of filename to be matched/ranked
  // Out: int representing rank value (-1 for valueless match)
  // Out: String is modified; matched bit is chopped off
  // Out: bool, did we match or not?
  virtual bool MatchAndRankComponent(FGString& fnameRemainder,
                                     int* pMatchVal) const = 0;
  // Need virtual destructor for the mm
  virtual ~FGFileNameComponent();
};

#endif // _FGFNCOMP_H
