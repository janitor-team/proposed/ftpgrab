#ifndef _FGEXC_H
#define _FGEXC_H

// fgexc.h
//
// Header file for exception class

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGException {
public:
  // Enumerated exception types
  enum EFGExceptions {
    // Connection failures
    kHostResolveFail = 0,
    kHostResolveRetryableFail,
    kConnectResourceAllocFail,
    kConnectRefused,
    kConnectHostUncontactable,
    kConnectMiscFail,
    kConnectLost,
    // FTP protocol parse failures
    kResponseTooLong,
    kResponseMalformed,
    kResponseUnexpected,
    // FTP server doesn't like us failures
    kAccessDenied,
    kServerFull,
    // When we can't find stuff we expect
    kNoSuchDir,
    kNoSuchFile,
    // Local file handling issues
    kLocalNoSuchDir,
    kLocalNoSuchFile,
    kLocalPermissionDenied,
    // Timeout d/loading file
    kDownloadTimeout,
    // Config file parse failures
    kConfigFileNotFound,
    kConfigMissingColon,
    kDuplicateToken,
    kUnexpectedEndOfConfig,
    // Misc. filesystem stuff
    kFileCreateFailed,
    kConcurrentGrabs,
    kUnlinkFailed,
    kWriteFailed,
    // Log file failures
    kLogFileOpenFailed,
    // Failures parsing filenames/components
    kParseEndOfString,
    kParseUnexpectedChar,
    kUnrecognizedComponent,
    // Duff user supplied regular expression
    kInvalidRegexp,
    // Problems reading file from network
    kNetworkReadError,
    kFileTooShort,
  };

  // Constructor - pass in reason for throwing exception
  // plus additional details message
  FGException(EFGExceptions reason, const FGString& details);
  // Constructor where no additional details available
  FGException(EFGExceptions reason);

  EFGExceptions GetReason(void) const;

  // Methods to analyze severity of exception/what went wrong etc.
  bool IsConditionRetryable(void) const;
  // Severity is 0-10
  int GetSeverity(void) const;
  FGString GetFullDetails(void) const;

private:
  // Internal storage details
  EFGExceptions mWhat;
  FGString mDetails;
};

#endif // _FGEXC_H
