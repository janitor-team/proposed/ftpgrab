// fgexc.cc

#include "fgexc.h"

// String table of exception messages
const char* const exceptionTexts[] =
  {"Hostname lookup failed",
   "Hostname lookup failed",
   "Couldn't allocate resources to connect",
   "Connection refused at remote site",
   "No response from remote site",
   "Failed to connect to remote site",
   "Lost connection to remote site",
   "FTP response too large!",
   "FTP response malformed!",
   "FTP response code unexpected!",
   "FTP server refuses connection",
   "FTP server is full",
   "Remote directory not found",
   "Remote file not found",
   "Local directory not found",
   "Local file not found",
   "Permission denied (local)",
   "Timeout downloading file",
   "Cannot open config file",
   "Missing `:' in config file",
   "Duplicate token in config file",
   "Config file ended in middle of rule",
   "Failed to create local file",
   "Detected concurrent download of same file",
   "Failed to unlink local file",
   "I/O error writing local file",
   "Could not open/create log file",
   "Unexpected end of string parsing filename rule",
   "Unexpected character parsing filename rule",
   "Unrecognized component token",
   "Invalid regular expression",
   "Error reading from network",
   "Unexpected file EOF (remote died?)"
};

FGException::FGException(EFGExceptions reason, const FGString& details)
: mWhat(reason), mDetails(details)
{
}

FGException::FGException(EFGExceptions reason)
: mWhat(reason)
{
}

FGException::EFGExceptions
FGException::GetReason(void) const
{
  return mWhat;
}

FGString
FGException::GetFullDetails(void) const
{
  FGString ret(exceptionTexts[(int)mWhat]);
  if (!mDetails.IsEmpty()) {
    ret += ": ";
    ret += mDetails;
  }

  return ret;
}
