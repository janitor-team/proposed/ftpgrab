// fgcharcomp.cc

#include "fgcharcomp.h"

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

#include <ctype.h>

FGCharacterComponent::FGCharacterComponent(bool isOptional)
{
  mIsOptional = isOptional;
}

FGCharacterComponent::~FGCharacterComponent()
{
}

bool
FGCharacterComponent::MatchAndRankComponent(FGString& fnameRemainder,
                                            int* pMatchVal) const
{
  // Initialize to be safe
  *pMatchVal = -1;

  // Sanity checks
  if (fnameRemainder.GetLength() < 1 || !isalpha(fnameRemainder[0])) {
    // May or may not be a "match" or such. If the character component
    // is NOT optional, it's not match. Otherwise it IS a match, with
    // minimal value
    if (!mIsOptional)
    {
      return false;
    }
    else
    {
      *pMatchVal = 0;
      return true;
    }
  }

  // The value returned will actually not be zero based, it will
  // be ASCII "A" based (65??)
  // It is of no conseqence
  *pMatchVal = toupper(fnameRemainder[0]);

  // Chop off the digits we consumed
  int charsLeft = fnameRemainder.GetLength() - 1;
  FGString bitLeft = fnameRemainder.Right(charsLeft);

  fnameRemainder = bitLeft;

  return true;
}
