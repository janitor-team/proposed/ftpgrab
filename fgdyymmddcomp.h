#ifndef _FGDYYMMDD_H
#define _FGDYYMMDD_H

// fgdyyddmm.h
//
// FGDateYYMMDDComponent
//
// Class to handle matching and ranking of date components
// e.g. Wine-990328.tar.gz etc.

#ifndef _FGFNCOMP_H
#include "fgfncomp.h"
#endif

class FGDateYYMMDDComponent : public FGFileNameComponent {
public:
  // Default constructor
  FGDateYYMMDDComponent();

  virtual bool MatchAndRankComponent(FGString& fnameRemainder,
                                     int* pMatchVal) const;
  virtual ~FGDateYYMMDDComponent();

private:
  // Banned!
  FGDateYYMMDDComponent(const FGDateYYMMDDComponent& other);
  FGDateYYMMDDComponent& operator=(const FGDateYYMMDDComponent& other);
};

#endif // _FGDYYMMDD_H
