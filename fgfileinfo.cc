// fgfileinfo.cc

#include "fgfileinfo.h"

FGFileInfo::FGFileInfo(const FGString& filename)
: mFileName(filename), mSize(-1), mIsDir(false), mIsRegularFile(true)
{
}

FGFileInfo::FGFileInfo(const FGString& filename, int size,
                       bool isDir, bool isFile)
: mFileName(filename), mSize(size), mIsDir(isDir), mIsRegularFile(isFile)
{
}

FGFileInfo::FGFileInfo()
: mSize(-1), mIsDir(false), mIsRegularFile(true)
{
}

bool
FGFileInfo::operator==(const FGFileInfo& other) const
{
  return (mFileName == other.mFileName &&
          mIsRegularFile == other.mIsRegularFile);
}

const FGString&
FGFileInfo::GetFileName(void) const
{
  return mFileName;
}

bool
FGFileInfo::IsRegularFile(void) const
{
  return mIsRegularFile;
}

int
FGFileInfo::GetSize(void) const
{
  return mSize;
}
