#ifndef _FGDLACTION_H
#define _FGDLACTION_H

// fgdlaction.h
//
// Implementation class of the download action

#include "fgactioni.h"

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGConnectionInterface;

class FGDownloadAction : public FGActionInterface {
public:
  // Construct with filename to download
  FGDownloadAction(const FGString& fname, FGConnectionInterface* pConnIf,
                   const FGString& localDir, int size = -1);

  ~FGDownloadAction();

  // Overridden virtual Do method
  virtual void VirtualDo(void) const;

  // And Abort()
  virtual void Abort(void) const;

private:
  // Banned!!
  FGDownloadAction(const FGDownloadAction& other);
  FGDownloadAction& operator=(const FGDownloadAction& other);

  FGConnectionInterface* mpConnIf;
  FGString mFileName;
  FGString mLocalDir;

  // File descriptor pointing to the local file copy we are downloading
  int mDestFD;

  // Expected size of the file we are downloading
  int mFileSize;
};

#endif // _FGDLACTION_H
