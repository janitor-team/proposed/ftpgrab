// fgdlist.cc

#include "fgdlist.h"

#ifndef _FGSTING_H
#include "fgstring.h"
#endif

FGDirListing::FGDirListing(const FGString& dirName)
: mDirName(dirName)
{
}

FGDirListing
FGDirListing::GetRHSOnlyFiles(const FGDirListing& lhs,
                              const FGDirListing& rhs)
{
  FGDirListing ret("NODIR");

  // Could be dealing with 100's or 1000's of files. Should really use
  // map (tree based) not hopeless vector iteration. Heard of O(n^2)? :-)

  std::vector<FGFileInfo>::const_iterator iRHSFiles;
  for (iRHSFiles = rhs.begin(); iRHSFiles != rhs.end(); iRHSFiles++) {
    if (iRHSFiles->IsRegularFile()) {
      ret.push_back(*iRHSFiles);
    }
  }

  std::vector<FGFileInfo>::const_iterator iLHSFiles;
  for (iLHSFiles = lhs.begin(); iLHSFiles != lhs.end(); iLHSFiles++) {
    // For each LHS files scan through current list for match
    // Ugh slow slow need a tree based thing see above comment
    std::vector<FGFileInfo>::iterator iCurFiles;
    for (iCurFiles = ret.begin(); iCurFiles != ret.end(); iCurFiles++) {
      if (*iLHSFiles == *iCurFiles) {
        // Match so waste it from return list
        ret.erase(iCurFiles);
        break;
      }
    }
  }

  return ret;
}

const FGString&
FGDirListing::GetDirName(void) const
{
  return mDirName;
}
