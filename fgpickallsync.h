#ifndef _FGPICKALLSYNC_H
#define _FGPICKALLSYNC_H

// fgpickallsync.h
//
// Implementation of a file picker that selects all remote files for
// which we do not have local copies

#ifndef _FGFPICKI_H
#include "fgfpicki.h"
#endif

class FGConnectionInterface;

class FGPickAllSync : public FGFilePickerInterface {
public:
  // Must be constructed with connection interface
  FGPickAllSync(FGConnectionInterface* pConnIf);

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir);
private:
  // Banned!
  FGPickAllSync(const FGPickAllSync& other);
  FGPickAllSync& operator=(const FGPickAllSync& other);
};

#endif // _FGPICKALLSYNC_H
