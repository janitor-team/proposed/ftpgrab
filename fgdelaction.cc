// fgdelaction.cc

#include "fgdelaction.h"

#ifndef _FGFSHELP_H
#include "fgfshelp.h"
#endif

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

#ifndef _FGEXC_H
#include "fgexc.h"
#endif

FGDeleteAction::FGDeleteAction(const FGString& fname,
                               const FGString& localDir)
: mFileName(fname), mLocalDir(localDir)
{
}

FGDeleteAction::~FGDeleteAction()
{
}

void
FGDeleteAction::VirtualDo(void) const
{
  // Just nuke the file
  FGString file = mLocalDir;
  file += '/';
  file += mFileName;
  // If we get a "not found" exception, ignore it
  // This is because if something else deletes it, we don't care. We
  // only care that the file is _gone_ :)
  // I introduced this because two threads can race such that two of
  // them notice the same file needs wasting
  try {
    FGFileSystemHelper::DeleteFile(file);
  }
  catch (FGException& e) {
    if (e.GetReason() != FGException::kLocalNoSuchFile) {
      throw;
    }
  }
}

void
FGDeleteAction::Abort(void) const
{
}
