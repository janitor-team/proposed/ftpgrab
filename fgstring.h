#ifndef _FGSTRING_H
#define _FGSTRING_H

// fgstring.h
//
// My own C++ string class
// Because: the only one I can find on my system is woefully inadequate

class FGString {
public:
  // Constructors
  // Default constructor
  FGString();
  // Construct from const char*
  FGString(const char* pStr);
  // Copy constructor
  FGString(const FGString& other);

  // Assignment op.
  FGString& operator=(const FGString& other);

  // Destructor
  ~FGString();

  // Comparison operators
  bool operator==(const FGString& other) const;
  bool operator!=(const FGString& other) const;

  // Casts
  // Cast to a const char*
  operator const char*() const;

  // Concatenation
  void operator+=(const FGString& other);
  void operator+=(char other);

  // Get methods
  unsigned int GetLength(void) const;
  char operator[](int pos) const;

  // Substring methods
  FGString Left(unsigned int num) const;
  FGString Right(unsigned int num) const;

  bool Contains(char c) const;

  // Conversions
  int AToI(void) const;

  bool IsEmpty(void) const;
  void MakeEmpty(void);

private:
  // Internals
  char* mpString;
  unsigned int mLength;
};

#endif // _FGSTRING_H
