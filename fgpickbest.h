#ifndef _FGPICKBEST_H
#define _FGPICKBEST_H

// fgpickbest.h
//
// Implementation of a file picker that selects the most "recent"
// version(s) 

#ifndef _FGFPICKI_H
#include "fgfpicki.h"
#endif

class FGString;
class FGConnectionInterface;

class FGPickBest : public FGFilePickerInterface {
public:
  // Must be constructed with connection interface
  // and a count of how many revisions of a file to
  // retain locally, e.g. 2 => best match and previous
  // best match
  // Also should be constructed with match string
  FGPickBest(FGConnectionInterface* pConnIf, const FGString& match,
             int count);
  virtual ~FGPickBest();

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir);
private:
  // Banned!
  FGPickBest(const FGPickBest& other);
  FGPickBest& operator=(const FGPickBest& other);

  // Implementation details hidden by Cheshire cat
  struct Internal_FGPickBest;
  Internal_FGPickBest* mpInternals;
};

#endif // _FGPICKBEST_H
