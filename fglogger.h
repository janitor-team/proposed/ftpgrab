#ifndef _FGLOGGER_H
#define _FGLOGGER_H

// fglogger.h
// Simple class encapsulating the event logger

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

// Fun and games!!
struct _IO_FILE;
typedef struct _IO_FILE FILE;

class FGException;

class FGLogger {
public:
  // Enum of log severities
  enum EFGLogSeverity {
    kFGLLDebug = 1,
    kFGLLVerbose,
    kFGLLInfo,
    kFGLLWarn,
    kFGLLErr,
    kFGLLBadNews
  };

  // To shut up some warnings
  FGLogger();

  // The all important methods to log things
  void LogMsg(const FGString& msg, EFGLogSeverity severity,
              bool locked = false);
  void LogException(const FGException& e, bool locked = false);

  // Method to access the logger object - singleton pattern
  static FGLogger& GetLogger(void);

  // To set the log file name
  static void SetLogName(const FGString& name);

  // Special lock methods for client activity that needs to guarantee
  // sequentially logged messages appear uninterrupted by other thread
  // logging activity
  static void Lock(void);
  static void Unlock(void);

private:
  // Banned!
  FGLogger(const FGLogger& other);
  FGLogger& operator=(const FGLogger& other);

  // Construct the logger
  FGLogger(const FGString& fileName);

  // The actual log instance
  static FGLogger* mspLogger;

  // The filename to log to - must be set before first call to
  // GetLogger()
  static FGString msFileName;

  // Internal methods
  FGString GetDateStamp(void) const;

  // Stream we are logging to
  FILE* mpFile;

  // Helper classes
  // Urgh. Can't forward declare pthread_mutex_t => use of void*
  class MutexAutoUnlocker
  {
  public:
    MutexAutoUnlocker();
    void Prime(void* pMutex);
    ~MutexAutoUnlocker();
  private:
    void* mpMutex;
  };
};

#endif // _FGLOGGER_H
