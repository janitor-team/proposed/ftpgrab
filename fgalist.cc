// fgalist.cc

#include "fgalist.h"

// Action interface
#ifndef _FGACTIONI_H
#include "fgactioni.h"
#endif

void
FGActionList::DoActions(void) const
{
  std::vector<FGActionInterface*>::const_iterator iActions;

  for (iActions = begin(); iActions != end(); iActions++) {
    (*iActions)->Do();
  }
}

void
FGActionList::FreeResources(void)
{
  std::vector<FGActionInterface*>::iterator iActions;

  for (iActions = begin(); iActions != end(); iActions++) {
    delete *iActions;
  }
}
